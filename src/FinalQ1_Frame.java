import javafx.scene.control.ComboBox;

import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.regex.PatternSyntaxException;

public class FinalQ1_Frame extends JFrame {
    String[] group_type_main = {"群組分類", "undefined", "classmate", "family", "friend"};
    private JComboBox group_comboBox_main = new JComboBox(group_type_main);
    private JPanel contact_n_addContactPanel = new JPanel();
    private JPanel Q2Panel = new JPanel();
    private JPanel topPanel = new JPanel();
    private JPanel midPanel = new JPanel();
    private JButton searchButton = new JButton("Search");
    private JLabel contactLabel = new JLabel("Contacts");
    private JLabel addContactLabel = new JLabel("+");
    private JTextField searchTextField = new JTextField();
    private JScrollPane scrollPane;
    private int selectRowNumber = 0;
    private int temp1_selectRowNumber;
    private int temp2_selectRowNumber;
    private String selectName = "";
    private Font font1 = new Font("", Font.PLAIN, 30);
    private Font font2 = new Font("", Font.PLAIN, 30);
    private int mouseCount = 0;

    private static final String DATABASE_URL = "jdbc:mysql://localhost/member?autoReconnect=true&useSSL=false";
    private static final String USERNAME = "java";
    private static final String PASSWORD = "java";
    private static final String DEFAULT_QUERY = "SELECT name FROM people";

    private static ResultSetTableModel tableModel;
    private static ResultSetTableModel temp_tableModel;
    private JTable resultTable;
    private JTable temp_resultTable;
    private JFrame newContact;
    private JButton submit_Btn;
    private JComboBox type_comboBox;
    private JTextField name_JTextField;
    private JTextField phone_JTextField;
    private JComboBox group_comboBox;

    public FinalQ1_Frame(){
        super("Contacts");


        add(Q2Panel);
        Q2Panel.setLayout(new BorderLayout());
        midPanel.setLayout(new BoxLayout(midPanel, BoxLayout.X_AXIS));
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
        contact_n_addContactPanel.setLayout(new BorderLayout());

        midPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        midPanel.add(searchTextField);
        midPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        midPanel.add(searchButton);
        midPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        searchButton.addActionListener(new findButtonHandler());

        searchTextField.addActionListener(new ActionListener() {
            //按下Enter就會自動執行
            public void actionPerformed(ActionEvent e)
            {
                if (searchTextField.getText().length() == 0)
                    allDataDisplay();
                else
                {
                    try
                    {
                        findPerson(searchTextField.getText());
                    }
                    catch (PatternSyntaxException pse)
                    {
                        JOptionPane.showMessageDialog(null,
                                "Bad regex pattern", "Bad regex pattern",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }

        });

        addContact();
        group_comboBox_mainOnClick(); // 當群組分類comboBox 選取後，會執行此method. 讓清單列只顯示該群組的人

        contactLabel.setFont(font1);
        addContactLabel.setFont(font1);
        addContactLabel.setForeground(Color.BLUE);
        addContactLabel.setPreferredSize(new Dimension(40,40));
        contact_n_addContactPanel.add(contactLabel, BorderLayout.WEST);
        contact_n_addContactPanel.add(addContactLabel, BorderLayout.EAST);
        contact_n_addContactPanel.add(group_comboBox_main, BorderLayout.CENTER);
        topPanel.add(contact_n_addContactPanel);
        topPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        topPanel.add(midPanel);

        Q2Panel.add(topPanel, BorderLayout.NORTH);
        initialDataDisplay();

        Q2Panel.add(scrollPane, BorderLayout.CENTER);

        setGroup();

    }

    private void group_comboBox_mainOnClick() {
        group_comboBox_main.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (group_comboBox_main.getSelectedItem().toString().matches("群組分類")){

                    //SELECT name FROM people
                    try {
                        tableModel.setQuery("SELECT name FROM people");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }else if (group_comboBox_main.getSelectedItem().toString().matches("undefined")){

                    //SELECT name FROM people WHERE Group1 = 'undefined'
                    try {
                        tableModel.setQuery("SELECT name FROM people WHERE Group1 = 'undefined'");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }else if (group_comboBox_main.getSelectedItem().toString().matches("classmate")){

                    //SELECT name FROM people WHERE Group1 = 'classmate'
                    try {
                        tableModel.setQuery("SELECT name FROM people WHERE Group1 = 'classmate'");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }else if (group_comboBox_main.getSelectedItem().toString().matches("family")){

                    //SELECT name FROM people WHERE Group1 = 'family'
                    try {
                        tableModel.setQuery("SELECT name FROM people WHERE Group1 = 'family'");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }else if (group_comboBox_main.getSelectedItem().toString().matches("friend")){

                    //SELECT name FROM people WHERE Group1 = 'friend'
                    try {
                        tableModel.setQuery("SELECT name FROM people WHERE Group1 = 'friend'");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }
            }
        });
    }

    private void setGroup() {
        /****
        try {

            tableModel.resultSet.beforeFirst();
            while (tableModel.resultSet.next()) {
                tableModel.resultSet.moveToInsertRow();
                tableModel.resultSet.updateString(5, "undefined");
            }
            tableModel.resultSet.beforeFirst();

        } catch (SQLException e) {
            e.printStackTrace();
        }
         ****/

    }

    private void addContact() {
        addContactLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                newContact = new JFrame();
                newContact.setSize(400, 700);
                newContact.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                newContact.setVisible(true);
                newContactGUI();
                submit_Btn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        newContactCheckphoneType();
                    }
                });
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void newContactCheckphoneType() {
/****************************
        if (type_comboBox.getSelectedItem().toString().matches("company")
                || type_comboBox.getSelectedItem().toString().matches("home")){



        }else {

            phone_JTextField.getComponentCount();

        }
 **************************/

        newContact.setVisible(false);
    }

    private void newContactGUI() {
        JPanel ContactJPanel_outside = new JPanel();
        JPanel newContact_Panel = new JPanel();
        String[] phone_type = {"company", "home", "cell"};
        String[] group_type = {"undefined", "classmate", "family", "friend"};
        JLabel newContactJLabel = new JLabel("New Contact");
        JLabel name_JLabel = new JLabel("Enter name:");
        JLabel type_JLabel = new JLabel("Select type:");
        JLabel phone_JLabel = new JLabel("Enter phone:");
        JLabel group_JLabel = new JLabel("Select group:");
        name_JTextField = new JTextField();
        type_comboBox = new JComboBox(phone_type);
        phone_JTextField = new JTextField();
        group_comboBox = new JComboBox(group_type);
        submit_Btn = new JButton("Submit");
        Dimension TextFieldSize = new Dimension(150, 20);

        newContact_Panel.setLayout(new GridLayout(10, 1));

        newContactJLabel.setFont(font1);
        name_JTextField.setPreferredSize(TextFieldSize);
        type_comboBox.setPreferredSize(TextFieldSize);
        phone_JTextField.setPreferredSize(TextFieldSize);
        submit_Btn.setPreferredSize(new Dimension(40, 40));

        newContact_Panel.add(newContactJLabel);
        newContact_Panel.add(name_JLabel);
        newContact_Panel.add(name_JTextField);
        newContact_Panel.add(type_JLabel);
        newContact_Panel.add(type_comboBox);
        newContact_Panel.add(phone_JLabel);
        newContact_Panel.add(phone_JTextField);
        newContact_Panel.add(group_JLabel);
        newContact_Panel.add(group_comboBox);
        newContact_Panel.add(submit_Btn);

        ContactJPanel_outside.add(newContact_Panel);

        newContact.add(ContactJPanel_outside, BorderLayout.CENTER);
    }

    ////////////////////////////////////////////////
    private void initialDataDisplay() {//初始化顯示
        try {
            tableModel = new ResultSetTableModel(DATABASE_URL, USERNAME, PASSWORD,DEFAULT_QUERY);
            resultTable = new JTable(tableModel);
            resultTable.setFont(font2);
            resultTable.setRowHeight(40);
            scrollPane = new JScrollPane(resultTable);

            final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);//點擊欄位名稱排序
            resultTable.setRowSorter(sorter);
            resultTable.addMouseListener(new mouseAdapter());
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }
    }//end of method initialDataDisplay

    ////////////////////////////////////////////////
    public class mouseAdapter extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {//需要雙擊才會跳出訊息視窗
            mouseCount++;
            if (mouseCount == 1 ) {

                temp1_selectRowNumber = selectRowNumber = resultTable.rowAtPoint(e.getPoint());

            }
            else if (mouseCount==2 ) {

                temp2_selectRowNumber = selectRowNumber = resultTable.rowAtPoint(e.getPoint());

                if(temp1_selectRowNumber==temp2_selectRowNumber) {//點擊同個位置才算雙擊
                    selectName = resultTable.getValueAt(selectRowNumber, 0).toString();
                    selectPeople(selectName);
                    String message = "Type: " + temp_resultTable.getValueAt(0, 2).toString()
                            + "\nPhone no.: " + temp_resultTable.getValueAt(0, 3).toString();
                    JOptionPane.showMessageDialog(Q2Panel, message, selectName, JOptionPane.INFORMATION_MESSAGE);
                    mouseCount=0;
                }

                mouseCount=0;
            }

        }
    }//end of mouseAdapter

    ////////////////////////////////////////////////
    private class findButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            if (searchTextField.getText().length() == 0)//當你甚麼都沒輸入
                allDataDisplay();//顯示全部如同預設
            else
            {
                try
                {
                    findPerson(searchTextField.getText());
                }
                catch (PatternSyntaxException pse)
                {
                    JOptionPane.showMessageDialog(null,
                            "Bad regex pattern", "Bad regex pattern",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            mouseCount=0;
        }
    }//end of deleteButtonHandler

    ////////////////////////////////////////////////
    private void allDataDisplay() {
        try {
            tableModel.setQuery(DEFAULT_QUERY);
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }
    }//end of method allDataDisplay

    ////////////////////////////////////////////////
    public void findPerson(String searchText ) {
        try {
            String FIND_QUERY = "SELECT name FROM people WHERE "
                    + "name"
                    + " LIKE '%"+searchTextField.getText()+"%'";
            tableModel.setQuery(FIND_QUERY);
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }

    }//end of method addPerson

    ////////////////////////////////////////////////
    public void selectPeople(String searchText ) {
        try {
            String SELECT_QUERY = "SELECT * FROM people WHERE "
                    + "name"
                    + " LIKE '"+ selectName +"'";
            temp_tableModel = new ResultSetTableModel(DATABASE_URL, USERNAME, PASSWORD, SELECT_QUERY);
            temp_resultTable = new JTable(temp_tableModel);
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }

    }//end of method addPerson
}
